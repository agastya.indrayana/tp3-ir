import lightgbm
import random
from tqdm import tqdm
import lightgbm as lgb
import numpy as np
from gensim.corpora import Dictionary
from gensim.models import LsiModel, TfidfModel
from scipy.spatial.distance import cosine
from lightgbm import LGBMRanker

NUM_NEGATIVES = 1


class Ranker:
    NUM_LATENT_TOPICS = 200
    documents = {}
    queries = {}
    dictionary = None
    model = None
    ranker: LGBMRanker = None

    def predict(self, X) -> None:
        return self.ranker.predict(X)

    def __init__(self) -> None:
        self._load_documents()
        self._build_lsi_model()
        self._train()

    def vector_rep(self, text):
        rep = [topic_value for (_, topic_value)
               in self.model[self.dictionary.doc2bow(text)]]
        return rep if len(rep) == self.NUM_LATENT_TOPICS else [0.] * self.NUM_LATENT_TOPICS

    def features(self, query, doc):
        v_q = self.vector_rep(query)
        v_d = self.vector_rep(doc)
        q = set(query)
        d = set(doc)
        cosine_dist = cosine(v_q, v_d)
        jaccard = len(q & d) / len(q | d)
        return v_q + v_d + [jaccard] + [cosine_dist]

    def _load_documents(self):
        with open("dev/nfcorpus/train.docs") as file:
            for line in tqdm(file):
                doc_id, content = line.split("\t")
                self.documents[doc_id] = content.split()
        with open("dev/nfcorpus/train.vid-desc.queries", encoding="utf8") as file:
            for line in tqdm(file):
                q_id, content = line.split("\t")
                self.queries[q_id] = content.split()

    def _build_lsi_model(self):
        self.dictionary = Dictionary()
        bow_corpus = [self.dictionary.doc2bow(doc, allow_update=True)
                      for doc in self.documents.values()]
        self.model = LsiModel(bow_corpus, num_topics=self.NUM_LATENT_TOPICS)

    def _train(self):
        q_docs_rel = {}  # grouping by q_id terlebih dahulu
        with open("dev/nfcorpus/train.3-2-1.qrel", encoding="utf8") as file:
            for line in tqdm(file):
                q_id, _, doc_id, rel = line.split("\t")
                if (q_id in self.queries) and (doc_id in self.documents):
                    if q_id not in q_docs_rel:
                        q_docs_rel[q_id] = []
                    q_docs_rel[q_id].append((doc_id, int(rel)))
        group_qid_count = []
        dataset = []
        for q_id in q_docs_rel:
            docs_rels = q_docs_rel[q_id]
            group_qid_count.append(len(docs_rels) + NUM_NEGATIVES)
            for doc_id, rel in docs_rels:
                dataset.append(
                    (self.queries[q_id], self.documents[doc_id], rel))
            # tambahkan satu negative (random sampling saja dari documents)
            dataset.append((self.queries[q_id], random.choice(
                list(self.documents.values())), 0))
        X = []
        Y = []
        for (query, doc, rel) in tqdm(dataset):
            X.append(self.features(query, doc))
            Y.append(rel)
        # ubah X dan Y ke format numpy array
        X = np.array(X)
        Y = np.array(Y)
        self.ranker = lightgbm.LGBMRanker(
            objective="lambdarank",
            boosting_type="gbdt",
            n_estimators=100,
            importance_type="gain",
            metric="ndcg",
            num_leaves=40,
            learning_rate=0.02,
            max_depth=-1)
        # di contoh kali ini, kita tidak menggunakan validation set
        # jika ada yang ingin menggunakan validation set, silakan saja
        self.ranker.fit(X, Y,
                        group=group_qid_count,
                        verbose=10)
