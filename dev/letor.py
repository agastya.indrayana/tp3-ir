import lightgbm
import random
from tqdm import tqdm
import lightgbm as lgb
import numpy as np
from gensim.corpora import Dictionary
from gensim.models import LsiModel, TfidfModel
from scipy.spatial.distance import cosine

documents = {}
with open("dev/nfcorpus/train.docs") as file:
    for line in tqdm(file):
        doc_id, content = line.split("\t")
        documents[doc_id] = content.split()

queries = {}
with open("dev/nfcorpus/train.vid-desc.queries", encoding="utf8") as file:
    for line in tqdm(file):
        q_id, content = line.split("\t")
        queries[q_id] = content.split()

# test untuk melihat isi dari 2 query
# print(queries["PLAIN-2428"])
# print(queries["PLAIN-2435"])

# melalui qrels, kita akan buat sebuah dataset untuk training
# LambdaMART model dengan format
#
# [(query_text, document_text, relevance), ...]
#
# relevance awalnya bernilai 1, 2, 3 --> tidak perlu dinormalisasi
# biarkan saja integer (syarat dari library LightGBM untuk
# LambdaRank)
#
# relevance level: 3 (fully relevant), 2 (partially relevant), 1 (marginally relevant)

NUM_NEGATIVES = 1

q_docs_rel = {}  # grouping by q_id terlebih dahulu
with open("dev/nfcorpus/train.3-2-1.qrel", encoding="utf8") as file:
    for line in tqdm(file):
        q_id, _, doc_id, rel = line.split("\t")
        if (q_id in queries) and (doc_id in documents):
            if q_id not in q_docs_rel:
                q_docs_rel[q_id] = []
            q_docs_rel[q_id].append((doc_id, int(rel)))

# group_qid_count untuk model LGBMRanker
group_qid_count = []
dataset = []
for q_id in q_docs_rel:
    docs_rels = q_docs_rel[q_id]
    group_qid_count.append(len(docs_rels) + NUM_NEGATIVES)
    for doc_id, rel in docs_rels:
        dataset.append((queries[q_id], documents[doc_id], rel))
    # tambahkan satu negative (random sampling saja dari documents)
    dataset.append((queries[q_id], random.choice(list(documents.values())), 0))

# test
# print("number of Q-D pairs:", len(dataset))
# print("group_qid_count:", group_qid_count)
# assert sum(group_qid_count) == len(dataset), "ada yang salah"
# print(dataset[:2])


# bentuk dictionary, bag-of-words corpus, dan kemudian Latent Semantic Indexing
# dari kumpulan 3612 dokumen.
NUM_LATENT_TOPICS = 200

dictionary = Dictionary()
bow_corpus = [dictionary.doc2bow(doc, allow_update=True)
              for doc in documents.values()]
model = LsiModel(bow_corpus, num_topics=NUM_LATENT_TOPICS)  # 200 latent topics

# test melihat representasi vector dari sebuah dokumen & query


def vector_rep(text):
    rep = [topic_value for (_, topic_value) in model[dictionary.doc2bow(text)]]
    return rep if len(rep) == NUM_LATENT_TOPICS else [0.] * NUM_LATENT_TOPICS


print(vector_rep(documents["MED-329"]))
print(vector_rep(queries["PLAIN-2435"]))


# kita ubah dataset menjadi terpisah X dan Y
# dimana X adalah representasi gabungan query+document,
# dan Y adalah label relevance untuk query dan document tersebut.
#
# Bagaimana cara membuat representasi vector dari gabungan query+document?
# cara simple = concat(vector(query), vector(document)) + informasi lain
# informasi lain -> cosine distance & jaccard similarity antara query & doc


def features(query, doc):
    v_q = vector_rep(query)
    v_d = vector_rep(doc)
    q = set(query)
    d = set(doc)
    cosine_dist = cosine(v_q, v_d)
    jaccard = len(q & d) / len(q | d)
    return v_q + v_d + [jaccard] + [cosine_dist]


X = []
Y = []
for (query, doc, rel) in tqdm(dataset):
    X.append(features(query, doc))
    Y.append(rel)


# ubah X dan Y ke format numpy array
X = np.array(X)
Y = np.array(Y)

print(X.shape)
print(Y.shape)


ranker = lightgbm.LGBMRanker(
    objective="lambdarank",
    boosting_type="gbdt",
    n_estimators=100,
    importance_type="gain",
    metric="ndcg",
    num_leaves=40,
    learning_rate=0.02,
    max_depth=-1)

# di contoh kali ini, kita tidak menggunakan validation set
# jika ada yang ingin menggunakan validation set, silakan saja
ranker.fit(X, Y,
           group=group_qid_count,
           verbose=10)


# unseen qd pair
query = "halo siapa nama mu?"

X_unseen = []
with open("dev/nfcorpus/train.docs") as file:
    for line in tqdm(file):
        doc_id, content = line.split("\t")
        documents[doc_id] = content.split()
        X_unseen.append(features(query.split(), content.split()))

# bentuk ke format numpy array
X_unseen = np.array(X_unseen)

# hitung scores
scores = ranker.predict(X_unseen)
print(scores)

# Ranking pada SERP

# sekedar pembanding, ada bocoran: D3 & D5 relevant, D1 & D4 partially relevant, D2 tidak relevan
# apakah LambdaMART berhasil merefleksikan hal ini?

did_scores = [x for x in zip([did for (did, _) in documents.items()], scores)]
sorted_did_scores = sorted(did_scores, key = lambda tup: tup[1], reverse = True)

print("query        :", query)
print("SERP/Ranking :")
for (did, score) in sorted_did_scores[:10]:
  print(did, score)