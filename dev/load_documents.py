from tqdm import tqdm
from apps.home.models import Document


s_docs = []
with open("dev/nfcorpus/train.docs") as file:
    for line in tqdm(file):
        doc_id, content = line.split("\t")
        s_docs.append(Document(doc_id=doc_id, content=content))


Document.objects.bulk_create(s_docs)
